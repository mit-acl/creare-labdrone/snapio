/**
 * @file misc.h
 * @brief Miscellaneous APIs to support SnapIO APPs<->sDSP comms
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <cstdint>
#include <cstdio>

namespace acl {
namespace snapio {

uint8_t* rpc_shared_mem_alloc(size_t bytes);
void rpc_shared_mem_free(uint8_t* ptr);
void voxl_rpc_shared_mem_deinit();

} // ns snapio
} // ns acl