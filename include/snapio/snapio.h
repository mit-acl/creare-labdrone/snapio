/**
 * @file snapio.h
 * @brief API for accessing ioboard via sDSP
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#pragma once

#include <atomic>
#include <array>
#include <cstdint>
#include <mutex>
#include <thread>

#include "snapio-protocol/sio_serial.h"

namespace acl {
namespace snapio {

  /**
   * Minimum and maximum allowable ESC commands, in microseconds.
   */
  static constexpr uint16_t PWM_MIN_PULSE_WIDTH = 1000;
  static constexpr uint16_t PWM_MAX_PULSE_WIDTH = 2000;

  /**
   * UART port to device mapping - note the differences between APQ8074 and APQ8096
   */
  enum class UART { /*APQ8074*/ J15 /*= 1*/, J13 /*= 2*/, J12 /*= 3*/, J9 /*= 4*/,
                    /*APQ8096*/ J7 /*= 9*/, J10 /*= 7*/, J11 /*= 12*/, /*J12 = 5*/ };

  class SnapIO
  {
  public:
    /**
     * @brief      SnapIO constructor.
     *
     *        The specified UART may be different depending if you are using
     *        the blue sf (APQ8074) or the red sfpro (or green voxl).
     *
     *        The baudrate should match that of the ioboard.
     *
     * @param[in]  uart      The UART (indicated by connector) to use
     * @param[in]  baudrate  Baudrate of serial connection
     */
    SnapIO(UART uart, int baudrate = 921600);
    ~SnapIO();

    void set_motors(const std::array<uint16_t, 6>& pwm);

    /**
     * @brief      Set the gripper open or close.
     *
     * @param[in]  open  True if desired to open the gripper
     */
    void set_gripper(bool open);

  private:
    const int bus_; ///< UART (BLSP bus) to use
    const int baud_; ///< requested baudrate

    //\brief Message handling
    static constexpr int BUF_LEN = 32 * 1024; ///< big 32k read buffer
    static constexpr int BUF_PKT_LEN = BUF_LEN / sizeof(sio_serial_message_t); ///< number of messages that fit
    sio_serial_message_t* msgs_buf_; ///< buffer for BUF_PKT_LEN messages read from parser

    //\brief Threading
    std::atomic<bool> thread_stop_;
    std::mutex mtx_;
    std::thread read_thread_;

    /**
     * @brief      UART reader, called in a separate thread
     */
    void read_thread();

  };

} // ns snapio
} // ns acl
