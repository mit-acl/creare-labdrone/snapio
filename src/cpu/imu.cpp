/**
 * @file imu.cpp
 * @brief API for accessing IMU (via imu_app api)
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <iostream>
#include <stdexcept>
#include <string>
#include <sstream>

#include <sensor-imu/sensor_imu_api.h>
#include <sensor-imu/sensor_datatypes.h>

#include "snapio/imu.h"

namespace acl {
namespace snapio {

IMU::IMU()
{
  // request handle to imu api
  imu_api_handle_ptr_ = sensor_imu_attitude_api_get_instance();
  if (imu_api_handle_ptr_ == nullptr) {
    throw std::runtime_error("Error getting IMU API handle");
  }

  // check that the version is compatible
  char* api_version = sensor_imu_attitude_api_get_version(imu_api_handle_ptr_);
  const std::string str_version{api_version};
  const std::string str_expected_version{SENSOR_IMU_AND_ATTITUDE_API_VERSION};
  if (str_version != str_expected_version) {
    std::stringstream ss;
    ss << "IMU API version mismatch: expected " << str_expected_version;
    ss << " but got " << str_version << ".";
#if defined(APQ8074)
    // the sensor-imu package provided for cross-compilation seems to be 1.2.2
    // while the version on-board the sf 8074 is 1.2.3
    std::cout << ss.str() << " Continuing on." << std::endl;
#else
    throw std::runtime_error(ss.str());
#endif
  }

  // initialize api
  int16_t api_rc = sensor_imu_attitude_api_initialize(imu_api_handle_ptr_,
                                            SENSOR_CLOCK_SYNC_TYPE_MONOTONIC);
  if (api_rc != 0) {
    throw std::runtime_error("Error: IMU API didn't initialize");
  }
  api_rc = sensor_imu_attitude_api_wait_on_driver_init(imu_api_handle_ptr_);
  if (api_rc != 0) {
    throw std::runtime_error("Error: IMU API driver init failed");
  }

  // start reading in separate thread
  thread_stop_ = false;
  read_thread_ = std::thread(&IMU::read_thread, this);
}

// ----------------------------------------------------------------------------

IMU::~IMU()
{
  thread_stop_ = true;
  if (read_thread_.joinable()) {
    read_thread_.join();
  }
  imu_api_handle_ptr_ = nullptr;
}

// ----------------------------------------------------------------------------

void IMU::register_imu_cb(Callback cb)
{
  std::lock_guard<std::mutex> lock(mtx_);
  cb_ = cb;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void IMU::read_thread()
{
  static constexpr int MAX_IMU_SAMPLES = 100;
  sensor_imu imu_buffer[MAX_IMU_SAMPLES];
  int32_t returned_sample_count = 0;
  int16_t api_rc;
  uint32_t prev_sequence_number = 0;
  uint32_t current_seqeunce_number;

  std::vector<Data> data;
  data.reserve(MAX_IMU_SAMPLES);

  while (!thread_stop_) {
    returned_sample_count = 0;
    // "this is a blocking call if no data available" - see sensor_imu_api.h
    api_rc = sensor_imu_attitude_api_get_imu_raw(imu_api_handle_ptr_, imu_buffer,
                                          MAX_IMU_SAMPLES, &returned_sample_count);
    if (api_rc != 0) {
      std::cout << "[WARN] IMU: Error getting samples from api" << std::endl;
    } else {
      if (returned_sample_count > 0) {
        current_seqeunce_number = imu_buffer[0].sequence_number;
        if (prev_sequence_number != 0 && prev_sequence_number + 1 != current_seqeunce_number) {
          std::cout << "[WARN] IMU: Missed samples, expected " << (prev_sequence_number+1)
                    << ", got " << current_seqeunce_number << ". Sample count: "
                    << returned_sample_count << std::endl;
        }
        prev_sequence_number = imu_buffer[returned_sample_count-1].sequence_number;

        data.clear();
        for (size_t i=0; i<returned_sample_count; i++) {
          data.push_back({});
          data.back().usec = imu_buffer[i].timestamp_in_us;
          data.back().seq = imu_buffer[i].sequence_number;
          data.back().acc_x = imu_buffer[i].linear_acceleration[0];
          data.back().acc_y = imu_buffer[i].linear_acceleration[1];
          data.back().acc_z = imu_buffer[i].linear_acceleration[2];
          data.back().gyr_x = imu_buffer[i].angular_velocity[0];
          data.back().gyr_y = imu_buffer[i].angular_velocity[1];
          data.back().gyr_z = imu_buffer[i].angular_velocity[2];
        }

        { // call handler if exists
          std::lock_guard<std::mutex> lock(mtx_);
          if (cb_) cb_(data);
        }
      }
    }
  }
  std::cout << "[INFO] IMU: Exiting read thread" << std::endl;
  sensor_imu_attitude_api_terminate(imu_api_handle_ptr_);
}

} // ns snapio
} // ns acl
