/**
 * @file gripper.cpp
 * @brief CLI tool to open / close gripper
 * @author Parker Lusk <plusk@mit.edu>
 * @date 29 April 2021
 */

#include <array>
#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include <snapio/snapio.h>

void help(int argc, char const *argv[])
{
  std::cout << argv[0] << " [1|0]" << std::endl;
  std::cout << "    " << "1 for open, 0 for close" << std::endl;
  if (argc == 2) {
    std::cout << std::endl;
    std::cout << "    " << "Got " << argv[1] << std::endl;
  }
  std::cout << std::endl;
}

int main(int argc, char const *argv[])
{
  if (argc != 2) {
    help(argc, argv);
    return -1;
  }

  bool open;

  int tmp;
  std::istringstream ss(argv[1]);

  if (!(ss >> tmp)) {
    help(argc, argv);
    return -2;
  } else {
    open = (tmp == 1);
  }

  acl::snapio::SnapIO sio(acl::snapio::UART::J12);
  sio.set_gripper(open);

  return 0;
}
