/**
 * @file read_imu.cpp
 * @brief CLI tool to read IMU
 * @author Parker Lusk <plusk@mit.edu>
 * @date 15 April 2021
 */

#include <future>
#include <iomanip>
#include <iostream>
#include <sstream>

#include <snapio/imu.h>

/// |brief Global variables
uint64_t last_t_us = 0;

void imu_cb(const std::vector<acl::snapio::IMU::Data>& samples)
{
  // std::cout << "Got " << samples.size() << " samples" << std::endl;
  for (const auto& s : samples) {
    const double dt = (s.usec - last_t_us) * 1e-6; // us to s
    const double hz = 1. / dt;
    last_t_us = s.usec;

    static constexpr int w = 5;
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2)
       << std::setw(w) << std::setfill(' ')
       << s.acc_x << ", "
       << std::setw(w) << std::setfill(' ')
       << s.acc_y << ", "
       << std::setw(w) << std::setfill(' ')
       << s.acc_z
       << '\t'
       << std::setw(w) << std::setfill(' ')
       << s.gyr_x << ", "
       << std::setw(w) << std::setfill(' ')
       << s.gyr_y << ", "
       << std::setw(w) << std::setfill(' ')
       << s.gyr_z;
    std::cout << s.usec << " us: (" << hz << " Hz): "
              << ss.str() << std::endl;
  }
}


int main(int argc, char const *argv[])
{
  acl::snapio::IMU imu;
  imu.register_imu_cb(imu_cb);

  // spin forever and let CPU do other things (no busy waiting)
  std::promise<void>().get_future().wait();
  return 0;
}
