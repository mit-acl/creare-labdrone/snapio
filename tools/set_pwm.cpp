/**
 * @file set_pwm.cpp
 * @brief CLI tool to send pwm commands
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 Oct 2019
 */

#include <array>
#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include <snapio/snapio.h>

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);

  // the parameters of the ramp (default is a constant-amplitude ramp)
  uint16_t usecs_low = 1000;
  uint16_t usecs_high = usecs_low;
  float ramp_secs = 0.0f;

  // the user does not want a ramp
  if (argc >= 2) {
    uint16_t tmp;
    std::istringstream ss(argv[1]);

    if (!(ss >> tmp)) {
      std::cerr << "Invalid number: " << argv[1] << std::endl;
    } else {
      usecs_low = tmp;
      usecs_high = tmp;
    }
  }

  // the user wants a ramp
  if (argc == 4) {

    {
      uint16_t tmp;
      std::istringstream ss(argv[2]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[2] << std::endl;
      } else {
        usecs_high = tmp;
      }
    }

    {
      float tmp;
      std::istringstream ss(argv[3]);
      if (!(ss >> tmp)) {
        std::cerr << "Invalid number: " << argv[3] << std::endl;
      } else {
        ramp_secs = std::fabs(tmp);
      }
    }

  }

  acl::snapio::SnapIO sio(acl::snapio::UART::J12, 921600);

  std::array<uint16_t, 6> pwm;
  pwm.fill(acl::snapio::PWM_MIN_PULSE_WIDTH);
  sio.set_motors(pwm);

  // the following is needed to allow the ESCs to start up.
  // some ESCs take a rather long time to initalize and wouldn't work otherwise.
  std::cout << "Set PWMs to 1000 to initialize ESCs\n";
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

  if (ramp_secs == 0.0f) {
    // constant pwm value
    std::cout << "Setting PWMs to " << usecs_low << std::endl;

    // command min pwm pulse width
    std::array<uint16_t, 6> pwm;
    pwm.fill(usecs_low);
    sio.set_motors(pwm);

    // avoid busy waiting (sleeping is better, but still not best)
    while (!stop) std::this_thread::sleep_for(std::chrono::milliseconds(100));
  } else {
    // pwm ramp
    std::cout << "Ramping PWMs from " << usecs_low  << " to ";
    std::cout << usecs_high << " in " << ramp_secs << " seconds";
    std::cout << std::endl;

    // sampling period (in milliseconds)
    constexpr int Tms = 50;

    // discretization based on user input
    const int N = std::round(ramp_secs / (Tms*1e-3));
    std::cout << N << std::endl;
    const uint16_t step = std::ceil((usecs_high - usecs_low)*1.0f / N);

    std::cout << "Discretization: " << N << " steps of " << step << std::endl;

    // powm pulse width command
    std::array<uint16_t, 6> pwm;
    pwm.fill(acl::snapio::PWM_MIN_PULSE_WIDTH);
    uint16_t current_pwm = usecs_low;

    int n = 0;
    while (!stop) {
      // ramp until high
      if (n++ <= N && current_pwm < (usecs_high + step)) {
        for (int i=0; i<6; ++i) pwm[i] = current_pwm;
        sio.set_motors(pwm);
        std::cout << "pwm: " << current_pwm << std::endl;

        std::this_thread::sleep_for(std::chrono::milliseconds(Tms));
        current_pwm += step;
      }

      // will busy wait once end of ramp
    }

  }

  return 0;
}
